/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */
package org.apache.logging.log4j.core.appender;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;


/**
 * Test for the ExtendedSmtpAppender.
 * ATTENTION: This test takes 1-2 minutes to execute because of summarizing time window!
 * Needs a specific configuration and system properties to configure SMTP, see log4j2-test.xml.
 * Sending and content of emails is not checked here, please do it on your own - hints are given
 * in the logs that are also output to console.
 * Note: Sending emails takes some time and some SMTP servers have a queue and delay sending a bit;
 *       the order of received emails may be different than log event + email creation.
 *       For a better check either use a fast local SMTP server, or use larger burstSummarizingSeconds.
 *
 * Note for Eclipse:  requires setting  Java Compiler/Annotations Processing  to active
 *     and  Factory Path  to  M2_REPO/org/apache/logging/log4j/log4j-core/2.11.0/log4j-core-2.11.0.jar
 *
 * @author Thies Wellpott (twapache@online.de)
 */
/*XXX  EXPECTED TEST RESULT:
Following Emails should be generated (test from 2018-08-16):
15:35:30.397-  1: [Test_ExOrigin] ERROR: alpha - first error log (should contain buffered lower level logs before)
15:35:30.598-  2: [Test_ExOrigin] ERROR: alpha - (nA)error message (no summarizing because of different exception) => java.lang.IllegalStateException: Dummy ISE 1 at org.apache...
15:35:30.805-  3: [Test_ExOrigin] ERROR: alpha - (nA)error message (no summarizing because of different exception) => java.lang.NullPointerException: Dummy NPE at org.apache.lo...
15:35:31.007-  4: [Test_ExOrigin] ERROR: alpha - (nA)error message (no summarizing because of different exception) => java.lang.RuntimeException: Dummy RuEx at org.apache.loggi...
15:35:31.209-  5: [Test_ExOrigin] ERROR: beta - (nB)error message (no summari+zing because of different message/30, even if same exception) => java.lang.IllegalStateException: Du...
15:35:31.416-  6: [Test_ExOrigin] ERROR: beta - (nB)error message (no summari#zing because of different message/30, even if same exception) => java.lang.IllegalStateException: Du...
15:35:31.617-  7: [Test_ExOrigin] ERROR: beta - (nB)error message (no summari^zing because of different message/30, even if same exception) => java.lang.IllegalStateException: Du...
15:35:31.819-  8: [Test_ExOrigin] ERROR: alpha - (nC)error message (no summarizing because of different exception origin) => java.lang.IllegalStateException: Dummy ISE 1 at org...
15:35:32.021-  9: [Test_ExOrigin] ERROR: alpha - (nC)error message (no summarizing because of different exception origin) => java.lang.IllegalStateException: Dummy ISE 1b at or...
15:35:32.222- 10: [Test_ExOrigin] ERROR: alpha - (nC)error message (no summarizing because of different exception origin) => java.lang.IllegalStateException: Dummy ISE 1c at or...
15:35:32.423- 11: [Test_ExOrigin] ERROR: alpha - 123(sA)error message (summarized, even if different loggers and contains different digits in msg and different text after first 3...
15:35:45.824- 14: [Test_ExOrigin] ERROR: beta - 78(sA)error message (summarized, even if different loggers and contains different digits in msg and different text after first 30 ...  [3x]
15:36:03.825- 15: [Test_ExOrigin] ERROR: alpha - 9(sA)error message (summarized, even if different loggers and contains different digits in msg and different text after first 30 ...
15:36:04.026- 16: [Test_ExOrigin] ERROR: beta - 5678(sA)error message (summarized, even if different loggers and contains different digits in msg and different text after first 3...
15:36:04.226- 17: [Test_ExOrigin] ERROR: beta - 12(sB)error message (summarized, even if contains different exception instances from same source) - first log OB (sent immediatly)...
15:36:04.630- 19: [Test_ExOrigin] ERROR: beta - 123(sB)error message (summarized, even if contains different exception instances from same source) - third log OB, last of summari...  [2x]
15:36:04.832- 20: [Test_ExOrigin] ERROR: alpha - (sC)error message (summarized,A because msg differs only after exactly 30 chars) - first log (sent immediatly)
15:36:05.233- 22: [Test_ExOrigin] ERROR: alpha - (sC)error message (summarized,C because msg differs only after exactly 30 chars) - third log (last of summarized)  [2x]
15:36:05.435- 23: [Test_LoggerRoot] ERROR: alpha - (nR)error message (no summarizing because of different loggers) - LRA
15:36:05.635- 24: [Test_LoggerRoot] ERROR: beta - (nR)error message (no summarizing because of different loggers) - LRB
15:36:05.835- 25: [Test_LoggerRoot] ERROR: thirdOne - (nR)error message (no summarizing because of different loggers) - LRC
15:36:06.036- 26: [Test_LoggerRoot] ERROR: alpha - (nS)error+ message (no summarizing because of different message/10, even if same logger) => java.lang.IllegalStateException: Du...
15:36:06.238- 27: [Test_LoggerRoot] ERROR: alpha - (nS)error# message (no summarizing because of different message/10, even if same logger) => java.lang.IllegalStateException: Du...
15:36:06.438- 28: [Test_LoggerRoot] ERROR: alpha - (nS)error^ message (no summarizing because of different message/10, even if same logger) => java.lang.IllegalStateException: Du...
15:36:06.640- 29: [Test_LoggerRoot] ERROR: beta - (nT)error message (no summarizing because of different root exception) - root1 => java.io.IOException: Dummy IOE with root 1 a...
15:36:06.843- 30: [Test_LoggerRoot] ERROR: beta - (nT)error message (no summarizing because of different root exception) - root2 => java.io.IOException: Dummy IOE with root 2 a...
15:36:07.043- 31: [Test_LoggerRoot] ERROR: beta - (nT)error message (no summarizing because of different root exception) - root3 => java.lang.RuntimeException: Dummy RuEx at or...
15:36:07.244- 32: [Test_LoggerRoot] ERROR: thirdOne - (sR)error message (summarized, because of same msg/10, logger name and root exception) - first, root1 => java.io.IOException...
2x  15:36:07.647- 34: [Test_LoggerRoot] ERROR: thirdOne - (sR)error message (summarized, because of same msg/10, logger name and root exception) - third, root1b => java.lang.IllegalS...
15:36:07.850- 35: [Test_LoggerRoot] ERROR: alpha - (sS) massive summarizing 420 events - #1
419x  15:36:09.639-454: [Test_LoggerRoot] ERROR: alpha - (sS) massive summarizing 420 events - #420
*/
@FixMethodOrder(org.junit.runners.MethodSorters.NAME_ASCENDING)		// is nicer for checking email order
public class ExtendedSmtpAppenderTest {

    // specific names for different test cases, see also Logger-config in log4j2-test.xml
    private static final Logger logOA = LogManager.getLogger("log4j.ExtendedSmtpAppenderTest_ExOrigin.alpha");
    private static final Logger logOB = LogManager.getLogger("log4j.ExtendedSmtpAppenderTest_ExOrigin.beta");

    private static final Logger logLRA = LogManager.getLogger("log4j.ExtendedSmtpAppenderTest_LoggerRoot.alpha");
    private static final Logger logLRB = LogManager.getLogger("log4j.ExtendedSmtpAppenderTest_LoggerRoot.beta");
    private static final Logger logLRC = LogManager.getLogger("log4j.ExtendedSmtpAppenderTest_LoggerRoot.thirdOne");


    // three exceptions created on the *same* source line (so do NOT reformat!); note: exception message does not matter
    private final Exception e1 = new IllegalStateException("Dummy ISE 1");    private final Exception e1same = new IllegalStateException("Dummy ISE 1same");    private final Exception e1same2 = new IllegalStateException("Dummy ISE 1same2");
    private final Exception e1b = new IllegalStateException("Dummy ISE 1b");		// same as above, but different source line
    private final Exception e1c = new IllegalStateException("Dummy ISE 1c");		// same as above, but different source line
    private final Exception e2 = new NullPointerException("Dummy NPE");
    private final Exception e3 = new RuntimeException("Dummy RuEx");
    // exceptions with a cause
    private final Exception e4r1 = new IOException("Dummy IOE with root 1", e1);
    private final Exception e4r2 = new IOException("Dummy IOE with root 2", e2);
    private final Exception e5r1b = new IllegalStateException("Dummy ISE with cause IAE + root 1b", new IllegalArgumentException("Dummy IAE with root 1b", e1b));
    //private final Exception e6r3 = new IOException("Dummy IOE with root 3", e3);



    private static void sleep(int tenthSeconds) {
        try {
            if (tenthSeconds >= 20) {
                System.out.println("sysout-INFO:  Sleeping " + (tenthSeconds/10) + " seconds...");
            }
            Thread.sleep(tenthSeconds * 100L);
        } catch (InterruptedException e) {
            // ignore
        }
    }

    /** Very short sleep, for use between logging creations to get emails (potentially) in nice order. */
    private static void shortSleep() {
        sleep(2);
    }


    // some logs at start/end for easier checking the output
    @BeforeClass
    public static void beforeClass() {
        LogManager.getLogger().info(" \nvvv--- BEGIN logging/sending emails ---vvv");
    }

    @AfterClass
    public static void afterClass() {
        LogManager.getLogger().info(" \n^^^--- END logging/sending emails  (now waiting to allow background sender to finish) ---^^^");
        // wait for background thread to safely finish end send emails (would be killed on JUnit test end!)
        sleep(190);
        System.out.println("sysout-INFO:  Finished ExtendedSmtpAppenderTest, all emails should have been send by background thread now.");
    }


    // NOTE:      (XXX - this just a "headline" marker)
    // logO* summarizes over 15s for Message/30, digits masked
    // + ExceptionClass (as default)
    // + ExceptionOrigin (class, method, source line of thrown)

    @Test
    public void test1ExClassOriginNoSummarize1() {
        // to have some buffered info before the error log (buffersize is 5)
        logOA.debug("just some A.debug in front (should NOT be part of email because of buffersize)");
        logOB.debug("just some B.debug in front (should be top of first error email)");
        logOB.trace("just some B.trace in front (should NOT be part of email because of level)");
        logOA.debug("another A.debug in front");
        logOA.info("just some A.info in front");
        logOB.info("just some B.info in front");
        logOB.warn("just some B.warn in front");

        logOA.error("first error log (should contain buffered lower level logs before)");
        shortSleep();
        String msg = "(nA)error message (no summarizing because of different exception)";
        // logger + message are the same
        logOA.error(msg, e1);
        shortSleep();
        logOA.error(msg, e2);
        shortSleep();
        logOA.error(msg, e3);
        shortSleep();
    }

    @Test
    public void test1ExClassOriginNoSummarize2() {
        // note: message (start) is different to other tests to prevent summarize conflict with it
        String msg = "(nB)error message (no summarizing because of different message/30, even if same exception)";
        String msgStart = msg.substring(0, 29),  msgEnd = msg.substring(29);
        // logger + exception are the same
        logOB.error(msgStart + "+" + msgEnd, e1);
        shortSleep();
        logOB.error(msgStart + "#" + msgEnd, e1);		// use easily recognizable character
        shortSleep();
        logOB.error(msgStart + "^" + msgEnd, e1);
        shortSleep();
    }

    @Test
    public void test1ExClassOriginNoSummarize3() {
        String msg = "(nC)error message (no summarizing because of different exception origin)";
        // logger + message are the same
        logOA.error(msg, e1);
        shortSleep();
        logOA.error(msg, e1b);
        shortSleep();
        logOA.error(msg, e1c);
        shortSleep();
    }


    @Test
    public void test2ExClassOriginWithSummarize1() {
        String msg = "(sA)error message (summarized, even if different loggers and contains different digits in msg and different text after first 30 chars)";
        // exception (no one) are the same
        logOA.error("123" + msg + " - first log OA (sent immediatly)");
        shortSleep();
        logOB.error("7" + msg + " - second log OB (first summarized one)");
        shortSleep();
        logOA.error("12345678" + msg + " - third log OA (not shown in summarized mail)");
        sleep(130);
        logOB.error("78" + msg + " - fourth log OB, last of summarized, ~13s later (so in summarizing time window)");
        sleep(180);		// a bit more than 15s to allow background thread to run again and clean up summarized info
        logOA.error("9" + msg + " - first log OA after another 18s (should come immediatly because of long time span)");
        shortSleep();
        logOB.error("5678" + msg + " - second log OB (is a single summarized log - with 'first log after another...')");
        shortSleep();
    }

    @Test
    public void test2ExClassOriginWithSummarize2() {
        String msg = "(sB)error message (summarized, even if contains different exception instances from same source)";
        // also different loggers and different digits in msg and different text after first 30 chars
        logOB.error("12" + msg + " - first log OB (sent immediatly)", e1);
        shortSleep();
        logOA.error("71" + msg + " - second log OA", e1same);
        shortSleep();
        logOB.error("123" + msg + " - third log OB, last of summarized)", e1same2);
        shortSleep();
    }

    @Test
    public void test2ExClassOriginWithSummarize3() {
        String msg = "(sC)error message (summarized, because msg differs only after exactly 30 chars)";
        String msgStart = msg.substring(0, 30),  msgEnd = msg.substring(30);
        logOA.error(msgStart + "A" + msgEnd + " - first log (sent immediatly)");
        shortSleep();
        logOA.error(msgStart + "B" + msgEnd + " - second log");
        shortSleep();
        logOA.error(msgStart + "C" + msgEnd + " - third log (last of summarized)");
        shortSleep();
    }


    // NOTE:      (XXX - this just a "headline" marker)
    // logLR* summarizes over 10s for Message/10 + LoggerName + only RootExceptionClass

    @Test
    public void test3LoggerRootNoSummarize1() {
        // to have some buffered info before the error log (buffersize is 3)
        logLRA.debug("just some A.debug in front (should NOT be part of email because of level)");
        logLRB.trace("just some B.trace in front (should NOT be part of email because of level)");
        logLRA.info("just some A.info in front (should NOT be part of email because of buffersize)");
        logLRA.warn("just some A.warn in front  (should be top of first error email)");
        logLRB.info("just some B.info in front");
        logLRA.trace("just some A.trace in front (should NOT be part of email because of level)");
        logLRB.warn("just some B.warn in front");

        String msg = "(nR)error message (no summarizing because of different loggers)";
        // message + exception (no one) are the same
        logLRA.error(msg + " - LRA");
        shortSleep();
        logLRB.error(msg + " - LRB");
        shortSleep();
        logLRC.error(msg + " - LRC");
        shortSleep();
    }

    @Test
    public void test3LoggerRootNoSummarize2() {
        String msg = "(nS)error message (no summarizing because of different message/10, even if same logger)";
        String msgStart = msg.substring(0, 9),  msgEnd = msg.substring(9);
        // logger + exception are the same
        logLRA.error(msgStart + "+" + msgEnd, e1);
        shortSleep();
        logLRA.error(msgStart + "#" + msgEnd, e1);		// use easily recognizable character
        shortSleep();
        logLRA.error(msgStart + "^" + msgEnd, e1);
        shortSleep();
    }

    @Test
    public void test3LoggerRootNoSummarize3() {
        String msg = "(nT)error message (no summarizing because of different root exception)";
        // logger + message are the same
        logLRB.error(msg + " - root1", e4r1);
        shortSleep();
        logLRB.error(msg + " - root2", e4r2);
        shortSleep();
        logLRB.error(msg + " - root3", e3);
        shortSleep();
    }


    @Test
    public void test4LoggerRootWithSummarize1() {
        String msg = "(sR)error message (summarized, because of same msg/10, logger name and root exception)";
        // logger + message are the same, root cause class name is the same
        logLRC.error(msg + " - first, root1", e4r1);
        shortSleep();
        logLRC.error(msg + " - second, directly e1", e1);
        shortSleep();
        logLRC.error(msg + " - third, root1b", e5r1b);
        shortSleep();
    }

    @Test
    public void test4LoggerRootWithSummarize2() {
        String msg = "(sS) massive summarizing 420 events";
        for (int i = 1;  i <= 420;  i++) {
            logLRB.info("Just some info log on LRB before, #{}", i);
            logLRC.warn("Just some warn log on LRC before (#{})", i);
            logLRA.error(msg + " - #" + i);
            if (i % 50 == 0) {
                // AsyncAppenders queue is of (default) size 1024 and would be filled up (3x420 > 1024)
                shortSleep();
            }
        }
    }

}
